<?php

require "connection/pdo.php";

$target_dir = "images2/";
$target_file = $target_dir . basename($_FILES["afbeelding"]["name"]);
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

if (move_uploaded_file($_FILES["afbeelding"]["tmp_name"], $target_file)) {
    echo "The file ". basename( $_FILES["afbeelding"]["name"]). " has been uploaded.";
} else {
    echo "Sorry, there was an error uploading your file.";
}

echo "<br><br>";
echo $target_file;

require "views/submit.view.php";