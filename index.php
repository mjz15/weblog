<?php

require "connection/pdo.php";

$sql="SELECT 
            berichten.id, 
            berichten.auteur_id,
            berichten.titel, 
            berichten.bericht, 
            berichten.datum,
            auteurs.naam
        FROM berichten 
        JOIN auteurs
        ON berichten.auteur_id = auteurs.id
        ORDER BY datum DESC";

require "views/index.view.php";