<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="../style.css">
</head>

<body>
    <a href="../index.php">Home</a>
    <h1>Berichten</h1>
    <ul>
        <?php foreach ($pdo->query($sql) as $row) : ?>
            <li>            
                <a href="controllers/bericht.php?id=<?php print ($row['id']); ?>">
                <h2><?= $row['titel'];?></a></h2>
                <i>Toegevoegd: </i>
                <?= $row['datum']; ?><br>
                <i>Auteur: </i>
                <?= $row['naam']; ?>
            </li>
        <?php endforeach ?>
    </ul>
    <button onclick="window.location.href='views/submit.view.php'">Nieuw bericht plaatsen</button>
    <button onclick="window.location.href='views/auteur.view.php'">Auteur toevoegen</button>
</body>
</html>