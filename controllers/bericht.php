<?php

require "../connection/pdo.php";

$bericht_id = $_GET['id'];

$r = $pdo->prepare(
    "SELECT 
        berichten.id, 
        berichten.auteur_id,
        berichten.titel, 
        berichten.bericht, 
        berichten.datum,
        afbeeldingen.afbeelding,
        reacties.reactie, 
        reacties.id
    FROM berichten 
    JOIN afbeeldingen
    JOIN reacties
    ON berichten.afbeelding_id = afbeeldingen.id
    WHERE berichten.id = $bericht_id
    ");
$r->execute();
$result = $r->fetch();

require "../views/bericht.view.php";  