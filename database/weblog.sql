-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server versie:                5.7.21-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Versie:              10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Databasestructuur van weblog wordt geschreven
CREATE DATABASE IF NOT EXISTS `weblog` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `weblog`;

-- Structuur van  tabel weblog.afbeeldingen wordt geschreven
CREATE TABLE IF NOT EXISTS `afbeeldingen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `afbeelding` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel weblog.afbeeldingen: ~22 rows (ongeveer)
/*!40000 ALTER TABLE `afbeeldingen` DISABLE KEYS */;
INSERT INTO `afbeeldingen` (`id`, `afbeelding`) VALUES
	(1, 'default-0499.jpg'),
	(2, 'images/nelly_nolta.jpg'),
	(3, 'images/willy_wonka.jpg'),
	(4, 'images/boris_becker.jpg'),
	(5, 'images/rintje_ritsma.jpg'),
	(6, 'images/tuin.jpg'),
	(7, 'images/tuin.jpg'),
	(8, 'rintje_ritsma.jpg'),
	(9, 'boris_becker.jpg'),
	(10, 'file.jpg'),
	(11, 'file.jpg'),
	(12, 'boris_becker.jpg'),
	(20, 'boris_becker.jpg'),
	(21, 'grumpy-cat.jpg'),
	(22, 'grumpycat.jpg'),
	(23, ''),
	(24, 'rintje_ritsma.jpg'),
	(25, '_89716241_thinkstockphotos-523060154.jpg'),
	(26, ''),
	(27, ''),
	(28, ''),
	(29, '');
/*!40000 ALTER TABLE `afbeeldingen` ENABLE KEYS */;

-- Structuur van  tabel weblog.auteurs wordt geschreven
CREATE TABLE IF NOT EXISTS `auteurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naam` text NOT NULL,
  `wachtwoord` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel weblog.auteurs: ~9 rows (ongeveer)
/*!40000 ALTER TABLE `auteurs` DISABLE KEYS */;
INSERT INTO `auteurs` (`id`, `naam`, `wachtwoord`) VALUES
	(1, 'Willy Wonka', 'chocolate'),
	(2, 'Boris Becker', 'safetyfirst'),
	(3, 'Nellie Nolta', 'tuin'),
	(4, 'Simon Sipma', 'ss'),
	(5, 'Rintje Ritsma', 'fryslanboppe'),
	(6, 'Charlie Chaplin', 'moderntimes'),
	(7, 'Hennie Huisman', 'soundmix'),
	(11, 'Dagobert Duck', 'geldgeldgeld'),
	(12, 'Govert Goudglans', 'RijkerDanDagobert');
/*!40000 ALTER TABLE `auteurs` ENABLE KEYS */;

-- Structuur van  tabel weblog.berichten wordt geschreven
CREATE TABLE IF NOT EXISTS `berichten` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auteur_id` int(11) NOT NULL DEFAULT '1',
  `categorie_id` int(11) NOT NULL DEFAULT '1',
  `titel` text NOT NULL,
  `bericht` text NOT NULL,
  `datum` datetime DEFAULT NULL,
  `afbeelding_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `auteur` (`auteur_id`),
  KEY `categorie` (`categorie_id`),
  KEY `afbeelding_id` (`afbeelding_id`),
  CONSTRAINT `FK_berichten_afbeeldingen` FOREIGN KEY (`afbeelding_id`) REFERENCES `afbeeldingen` (`id`),
  CONSTRAINT `FK_berichten_auteurs` FOREIGN KEY (`auteur_id`) REFERENCES `auteurs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel weblog.berichten: ~12 rows (ongeveer)
/*!40000 ALTER TABLE `berichten` DISABLE KEYS */;
INSERT INTO `berichten` (`id`, `auteur_id`, `categorie_id`, `titel`, `bericht`, `datum`, `afbeelding_id`) VALUES
	(20, 3, 1, 'Aanpassinkje', 'Oooooveral', '2019-05-23 22:27:24', 1),
	(64, 1, 1, 'Grumpy cat', 'Dit is Grumpy Cat. Grumpy Cat lijkt niet op Elvira. Die kijkt namelijk nooit grumpy. ', '2019-05-28 12:31:15', 22),
	(65, 1, 1, 'Grumpiest of cats', 'dfdfd', '2019-05-28 12:35:59', 1),
	(66, 1, 1, 'hj', 'hj', '2019-05-28 12:36:35', 1),
	(69, 1, 1, 'Suikerbuiker', 'Mmmm, lekker suiker in me buiker', '2019-05-28 12:42:33', 20),
	(70, 1, 1, 'dffd', 'fdfd', '2019-05-28 12:45:10', 1),
	(71, 1, 1, 'dfdfdsfa', 'dsfsdfsd', '2019-05-28 12:47:09', 1),
	(72, 1, 1, 'df', 'asdf', '2019-05-28 12:47:43', 1),
	(73, 1, 1, 'uiuiu', 'popopor', '2019-05-28 13:11:55', 22),
	(74, 1, 1, 'fgf', 'gfgf', '2019-05-28 13:26:46', 1),
	(75, 1, 1, 'dsgfdfg', 'sdfgsdfg', '2019-05-28 13:27:36', 1),
	(76, 1, 1, 'zxvc', 'zxcv', '2019-05-28 13:28:06', NULL);
/*!40000 ALTER TABLE `berichten` ENABLE KEYS */;

-- Structuur van  tabel weblog.cats wordt geschreven
CREATE TABLE IF NOT EXISTS `cats` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie` text,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel weblog.cats: ~5 rows (ongeveer)
/*!40000 ALTER TABLE `cats` DISABLE KEYS */;
INSERT INTO `cats` (`cat_id`, `categorie`) VALUES
	(1, 'Sport'),
	(2, 'Hobby'),
	(3, 'Wetenschap'),
	(4, 'Dieren'),
	(5, 'Star Trek');
/*!40000 ALTER TABLE `cats` ENABLE KEYS */;

-- Structuur van  tabel weblog.reacties wordt geschreven
CREATE TABLE IF NOT EXISTS `reacties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reactie` text NOT NULL,
  `bericht_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `bericht_id` (`bericht_id`),
  CONSTRAINT `FK_reacties_berichten` FOREIGN KEY (`bericht_id`) REFERENCES `berichten` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel weblog.reacties: ~1 rows (ongeveer)
/*!40000 ALTER TABLE `reacties` DISABLE KEYS */;
INSERT INTO `reacties` (`id`, `reactie`, `bericht_id`) VALUES
	(24, 'yada', 20);
/*!40000 ALTER TABLE `reacties` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
